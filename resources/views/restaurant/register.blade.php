@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">


@endsection

@section('subheader')


    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>Restaurant Owner Registration Form</h1>


            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Restaurant Owner Registration Form</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">

        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Increase your customers</h2>

        </div>
        <div class="row">
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
                <div class="feature">
                    <i class="icon_datareport"></i>
                    <h3><span>Increase</span> your sales</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
                <div class="feature">
                    <i class="icon_bag_alt"></i>
                    <h3><span>Delivery</span> or Takeaway</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
            {{--<div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
                <div class="feature">
                    <i class="icon_chat_alt"></i>
                    <h3><span>H24</span> chat support</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>--}}
        </div><!-- End row -->
        <div class="row">


            <div class="col-md-6 wow fadeIn" data-wow-delay="0.6s">
                <div class="feature">
                    <i class="icon_creditcard"></i>
                    <h3><span>Secure card</span> payment</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>

            {{-- <div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
                 <div class="feature">
                     <i class="icon_mobile"></i>
                     <h3><span>Mobile</span> support</h3>
                     <p>
                         Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                     </p>
                 </div>
             </div>--}}
        </div><!-- End row -->
        <div class="row">
            {{--<div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
                <div class="feature">
                    <i class="icon_wallet"></i>
                    <h3><span>Cash</span> payment</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>--}}

        </div><!-- End row -->
    </div><!-- End container -->

    <div class="white_bg">
        <div class="container margin_60_35">
            <div class="main_title margin_mobile">
                <h2 class="nomargin_top">Please submit the form below</h2>

            </div>
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <form  method="POST" action="{{ route('restaurant.register') }}">
                        {{ csrf_field() }}


                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Mahabub Jahan" value="{{ old('first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">Last name</label>
                                    <input type="text" class="form-control" id="name_contact" name="last_name" placeholder="Shuvo" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>



                        <div class="row">

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email</label>
                                    <input type="email" id="email" name="email" class="form-control " placeholder="shuvo@email.com" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                    <label for="mobile_number">Mobile number</label>
                                    <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="01722222222" value="{{ old('mobile_number') }}" required autofocus>
                                </div>

                                @if ($errors->has('mobile_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('restaurant_name') ? ' has-error' : '' }}">
                                    <label for="restaurant_name">Restaurant name</label>
                                    <input type="text" id="restaurant_name" name="restaurant_name" class="form-control" placeholder="Pizza King" value="{{ old('restaurant_name') }}" required autofocus>

                                    @if ($errors->has('restaurant_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city">City</label>
                                    <select name="city" class="form-control" id="city">

                                        <option value="" selected="">Select City</option>

                                        @foreach(\App\Http\Controllers\ManageAdmin\CityController::city() as $cities)
                                            <option value="{{ $cities->city_name }}">{{ $cities->city_name }}</option>
                                        @endforeach



                                    </select>

                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                        </div><!-- End row  -->



                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Create a password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password"  id="password" required autofocus>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password"  id="password_confirmation">
                                </div>
                            </div>
                        </div><!-- End row  -->


                        <div id="pass-info" class="clearfix"></div>

                        {{--<div class="row">
                            <div class="col-md-6">
                                <label><input name="term_&_condition" type="checkbox"  class="icheck" checked>Accept <a href="#">terms and conditions</a>.</label>
                            </div>
                        </div>--}}<!-- End row  -->

                        <hr style="border-color:#ddd;">
                        <div class="text-center">
                            <button class="btn_full_outline" type="submit">Submit</button>
                        </div>
                    </form>
                </div><!-- End col  -->
            </div><!-- End row  -->
        </div><!-- End container  -->
    </div><!-- End white_bg -->


    <!-- End Content =============================================== -->
@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')

@endsection
