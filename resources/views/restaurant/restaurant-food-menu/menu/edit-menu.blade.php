@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Edit Restaurant Menu</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')


    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                </li>
                <li>Edit Restaurant Menu</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="list_page.html" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" >Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('restaurant.menu') }}" class="active">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                    </ul>
                </div><!-- End box_style_1 -->


            </div><!-- End col-md-3 -->

            <div class="col-md-9">


                <div id="tabs" class="tabs">
                    <nav>
                        <ul>

                            <li><a href="#section-1" class="icon-profile"><span>Edit Menu</span></a></li>


                        </ul>
                    </nav>
                    <div class="content">





                        <section id="section-1">
                            <form method="POST" action="{{ url('/menu/update-menu').'/'.$menu->id }}">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon-edit-alt"></i>
                                    <h3>Edit Restaurant Menu</h3>

                                </div>
                                <hr class="styled_2">

                                <div class="wrapper_indent">



                                    <div class="form-group {{ $errors->has('menu_name') ? ' has-error' : '' }}">
                                        <label for="menu_name">Menu Name</label>
                                        <input class="form-control" name="menu_name" required id="menu_name" value="{{ $menu->menu_name }}" placeholder="Write Menu Name..Ex: DESSERTS" type="text">
                                        @if ($errors->has('menu_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('menu_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('menu_description') ? ' has-error' : '' }}">
                                        <label for="menu_description">Menu Description</label>
                                        <textarea class="form-control" name="menu_description" placeholder="Enter Description.." required id="menu_description" >{{ $menu->menu_description }}</textarea>
                                        @if ($errors->has('menu_description'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('menu_description') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group {{ $errors->has('publication_status') ? ' has-error' : '' }}">
                                        <label for="publication_status">Publication Status</label>
                                        <select class="form-control" id="publication_status" required name="publication_status">
                                            <option value="">Select Publication Status</option>
                                            <option <?php if($menu->publication_status == '1'){ echo "selected";} else echo ""?>  value="1">Published</option>
                                            <option <?php if($menu->publication_status == '0'){ echo "selected";} else echo ""?> value="0">Unpublished</option>
                                        </select>
                                        @if ($errors->has('publication_status'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('publication_status') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>



                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
