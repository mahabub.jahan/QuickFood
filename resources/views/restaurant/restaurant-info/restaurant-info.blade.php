@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/dropzone.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Restaurant Information</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                </li>
                <li>Restaurant Information</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" class="active" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}">Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}" >Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">


                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif



                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Edit Restaurant Details</span></a>
                            </li>


                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <form method="POST" action="{{ url('restaurant-info/edit') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon-restaurant"></i>
                                    <h3>General restaurant description</h3>

                                </div>

                                <div class="wrapper_indent">



                                    <div class="form-group {{ $errors->has('restaurant_name') ? ' has-error' : '' }}">
                                        <label for="restaurant_name">Restaurant name</label>
                                        <input class="form-control" name="restaurant_name" id="restaurant_name" required placeholder="Ex: Dhaka Food Corner"
                                               value="<?php if (! $restaurant->restaurant_name ) echo ""; else echo "$restaurant->restaurant_name";?>" type="text">
                                        <input type="hidden" name="id" value="{{ $restaurant->id }}">
                                        @if ($errors->has('restaurant_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group {{ $errors->has('restaurant_description') ? ' has-error' : '' }}">
                                        <label for="restaurant_description">About Your Restaurant</label>
                                        <textarea class="wysihtml5 form-control" name="restaurant_description"
                                                  placeholder="Enter text ..."
                                                  style="height: 200px;"> <?php if (! $restaurant->restaurant_description ) echo ""; else echo "$restaurant->restaurant_description";?></textarea>
                                        @if ($errors->has('restaurant_description'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_description') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('delivery_fee') ? ' has-error' : '' }}">
                                                <label for="delivery_fee">Min. Delivery Fee</label>
                                                <input type="text" id="delivery_fee" name="delivery_fee" required placeholder="Ex: ৳ 50"
                                                       value="<?php if ( ! $restaurant->delivery_fee  ) echo ""; else echo "$restaurant->delivery_fee";?>" class="form-control">
                                                @if ($errors->has('delivery_fee'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_fee') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('minimum_order') ? ' has-error' : '' }}">
                                                <label for="minimum_order">Minimum Order</label>
                                                <input type="text" id="minimum_order" name="minimum_order" required placeholder="Ex: ৳ 200"
                                                       value="<?php if ( ! $restaurant->minimum_order  ) echo ""; else echo "$restaurant->minimum_order";?>" class="form-control">
                                                @if ($errors->has('minimum_order'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('minimum_order') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('delivery_service') ? ' has-error' : '' }}">
                                                <label for="delivery_service">Delivery Service</label>
                                                <select class="form-control" name="delivery_service" id="delivery_service" required>
                                                    <option value="" selected>Select Delivery Service</option>
                                                    <option <?php if ( $restaurant->delivery_service == "yes" ) echo "selected"; else echo "";?> value="yes">Yes</option>
                                                    <option <?php if ( $restaurant->delivery_service == "no" ) echo "selected"; else echo "";?>  value="no">No</option>
                                                </select>
                                                @if ($errors->has('delivery_service'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_service') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('take_way_service') ? ' has-error' : '' }}">
                                                <label for="take_way_service">Take-way Service</label>
                                                <select class="form-control" name="take_way_service" id="take_way_service" required>
                                                    <option value="" >Select Take Way Service</option>
                                                    <option <?php if ( $restaurant->take_way_service == "yes" ) echo "selected"; else echo "";?> value="yes">Yes</option>
                                                    <option <?php if ( $restaurant->take_way_service == "no" ) echo "selected"; else echo "";?>  value="no">No</option>
                                                </select>
                                                @if ($errors->has('take_way_service'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('take_way_service') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('customer_care_number') ? ' has-error' : '' }}">
                                                <label for="customer_care_number">Customer Care Number</label>
                                                <input type="text" id="customer_care_number" name="customer_care_number" required placeholder="Ex: 01722222222"
                                                       value="<?php if ( ! $restaurant->customer_care_number  ) echo ""; else echo "$restaurant->customer_care_number";?>"
                                                       class="form-control">
                                                @if ($errors->has('customer_care_number'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_care_number') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group {{ $errors->has('customer_cate_service_duration') ? ' has-error' : '' }}">
                                                <label for="customer_cate_service_duration">Service Time Details</label>
                                                <input type="text" id="customer_cate_service_duration"  name="customer_cate_service_duration" required
                                                       value="<?php if ( ! $restaurant->customer_cate_service_duration  ) echo ""; else echo "$restaurant->customer_cate_service_duration";?>"
                                                       class="form-control" placeholder="Ex: Monday to Friday 9.00am - 7.30pm">
                                                @if ($errors->has('customer_cate_service_duration'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_cate_service_duration') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>





                                    </div>


                                </div><!-- End wrapper_indent -->

                                <hr class="styled_2">

                                <div class="indent_title_in">
                                    <i class="icon_pin_alt"></i>
                                    <h3>Address</h3>

                                </div>
                                <div class="wrapper_indent">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('restaurant_address') ? ' has-error' : '' }}">
                                                <label for="restaurant_address">Restaurant Address</label>
                                                <input type="text" id="restaurant_address" name="restaurant_address" required placeholder="Enter Restaurant Address..Ex:23/Dolfin Goli, Kolabagan, Dhaka"
                                                       value="<?php if ( ! $restaurant->restaurant_address  ) echo ""; else echo "$restaurant->restaurant_address";?>"
                                                       class="form-control">
                                                @if ($errors->has('restaurant_address'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_address') }}</strong>
                                    </span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-12 {{ $errors->has('city') ? ' has-error' : '' }}">
                                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="city">City</label>
                                                <select name="city" class="form-control" id="city" required>

                                                    <option value="" selected="">Select City</option>

                                                    @foreach(\App\Http\Controllers\ManageAdmin\CityController::city() as $cities)
                                                        <option <?php if($cities->city_name == $restaurant->city){ echo "selected";}else echo ""?> value="{{ $cities->city_name }}">{{ $cities->city_name }}</option>
                                                    @endforeach

                                                </select>

                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>




                                    </div><!--End row -->

                                    <div class="form-group {{ $errors->has('delivery_zone') ? ' has-error' : '' }}">
                                        <label for="delivery_zone">Delivery Zones</label>
                                        <textarea class="wysihtml5 form-control"  name="delivery_zone"
                                                  placeholder="Your Restaurant Delivery Zone ..." required
                                                  style="height: 200px;"><?php if (! $restaurant->delivery_zone ) echo ""; else echo "$restaurant->delivery_zone";?></textarea>
                                        @if ($errors->has('delivery_zone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('delivery_zone') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div><!-- End wrapper_indent -->

                                <hr class="styled_2">
                                <div class="indent_title_in">
                                    <i class="icon_images"></i>
                                    <h3>Logo and restaurant photos</h3>

                                </div>

                                <div class="wrapper_indent add_bottom_45">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">

                                                <label for="restaurant_logo">Upload Your restaurant Logo</label>
                                                <input type="file"  accept="image/*" id="restaurant_logo" name="restaurant_logo"  class="form-control">
                                                @if ($errors->has('restaurant_logo'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_logo') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <img src="{{asset($restaurant->restaurant_logo)}}" alt="Restaurant Logo" class="img-responsive" style="height: 80px; width: 80px">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="restaurant_image">Upload your restaurant Cover Picture</label>
                                                <input type="file" accept="image/*" id="restaurant_image"  name="restaurant_image"   class="form-control">
                                                @if ($errors->has('restaurant_image'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_image') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <img src="{{asset($restaurant->restaurant_image)}}" alt="Restaurant Logo" class="img-responsive" style="height: 80px; width: 170px">
                                            </div>
                                        </div>

                                    </div>




                                </div><!-- End wrapper_indent -->

                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1" style="text-align: center">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection





@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
    <script src="{{ asset('front-end') }}/js/dropzone.min.js"></script>


    <script>
        if ($('.dropzone').length > 0) {
            Dropzone.autoDiscover = false;
            $("#photos").dropzone({
                url: "upload",
                addRemoveLinks: true
            });

            $("#logo_picture").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });


        }
    </script>
@endsection
