@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/jvectormap/jquery-jvectormap.css">
@endsection
@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Manage Order</h1>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                </li>
                <li>Manage Order</li>
            </ul>
        </div>
    </div><!-- Position -->


    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" >Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}" class="active">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                @if($message = Session::get('destroy'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif
                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-menut-items"><span>Total Order Info</span></a></li>
                            <li><a href="#section-2" class="icon-menut-items"><span>Today Order</span></a></li>

                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <div class="indent_title_in">
                                <i class="icon-table"></i>
                                <h3>Order Info Data Table</h3>
                            </div>
                            <hr class="styled_2">

                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>SL No</th>
                                        <th>Order Id</th>
                                        <th>Customer Name</th>
                                        <th>Order Total</th>
                                        <th>Order Status</th>
                                        <th>Payment Type</th>
                                        <th>Payment Status</th>
                                        <th>Order Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($i=1)
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $order->id }}</td>
                                            <td>{{ $order->first_name.' '.$order->last_name }}</td>
                                            <td>{{ $order->order_total }}</td>

                                            <td>
                                                @if($order->order_status == "Pending")
                                                    <span class="label label-warning">{{ $order->order_status }}</span>
                                                @elseif($order->order_status == "Successful")
                                                    <span class="label label-success">{{ $order->order_status }}</span>
                                                @else
                                                    <span class="label label-danger">{{ $order->order_status }}</span>
                                                @endif

                                            </td>

                                            <td>{{ $order->payment_type }}</td>

                                            <td>
                                                @if($order->payment_status == "Pending")
                                                    <span class="label label-warning">{{ $order->payment_status }}</span>
                                                @elseif($order->payment_status == "Successful")
                                                    <span class="label label-success">{{ $order->payment_status }}</span>
                                                @else
                                                    <span class="label label-danger">{{ $order->payment_status }}</span>
                                                @endif

                                            </td>


                                            <td><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($order->delivery_schedule_day))->toFormattedDateString(); ?></td>
                                            <td>
                                                <a href="{{ url('/order/view-order-details/'.$order->id) }}" class="btn btn-info btn-xs" title="View Order Details">
                                                    <span class="glyphicon glyphicon-zoom-in"></span>
                                                </a>

                                                <a href="{{ url('/order/view-order-invoice/'.$order->id) }}" class="btn btn-warning btn-xs" title="View Order Invoice">
                                                    <span class="glyphicon glyphicon-zoom-out"></span>
                                                </a>
                                                <a href="{{ url('/pdf') }}" class="btn btn-success btn-xs" title="Download Invoice">
                                                    <span class="glyphicon glyphicon-download"></span>
                                                </a>
                                                <a href="{{ url('/order/edit-order/'.$order->id) }}" class="btn btn-primary btn-xs" title="Edit Order">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a href="{{ url('/order/delete-order/'.$order->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');" title="Order Delete">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->


                        </section><!-- End section 1 -->

                        <section id="section-2">
                            <div class="indent_title_in">
                                <i class="icon-table"></i>
                                <h3>Order Info Data Table</h3>
                            </div>
                            <hr class="styled_2">

                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>SL No</th>
                                        <th>Order Id</th>
                                        <th>Customer Name</th>
                                        <th>Order Total</th>
                                        <th>Order Status</th>
                                        <th>Payment Type</th>
                                        <th>Payment Status</th>
                                        <th>Order Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($i=1)
                                    @foreach($todayOrders as $todayOrder)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $todayOrder->id }}</td>
                                            <td>{{ $todayOrder->first_name.' '.$todayOrder->last_name }}</td>
                                            <td>{{ $todayOrder->order_total }}</td>

                                            <td>
                                                @if($todayOrder->order_status == "Pending")
                                                    <span class="label label-warning">{{ $todayOrder->order_status }}</span>
                                                @elseif($todayOrder->order_status == "Successful")
                                                    <span class="label label-success">{{ $todayOrder->order_status }}</span>
                                                @else
                                                    <span class="label label-danger">{{ $todayOrder->order_status }}</span>
                                                @endif

                                            </td>

                                            <td>{{ $todayOrder->payment_type }}</td>

                                            <td>
                                                @if($todayOrder->payment_status == "Pending")
                                                    <span class="label label-warning">{{ $todayOrder->payment_status }}</span>
                                                @elseif($todayOrder->payment_status == "Successful")
                                                    <span class="label label-success">{{ $todayOrder->payment_status }}</span>
                                                @else
                                                    <span class="label label-danger">{{ $todayOrder->payment_status }}</span>
                                                @endif

                                            </td>


                                            <td><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($todayOrder->delivery_schedule_day))->toFormattedDateString(); ?></td>
                                            <td>
                                                <a href="{{ url('/order/view-order-details/'.$todayOrder->id) }}" class="btn btn-info btn-xs" title="View Order Details">
                                                    <span class="glyphicon glyphicon-zoom-in"></span>
                                                </a>

                                                <a href="{{ url('/order/view-order-invoice/'.$todayOrder->id) }}" class="btn btn-warning btn-xs" title="View Order Invoice">
                                                    <span class="glyphicon glyphicon-zoom-out"></span>
                                                </a>
                                                <a href="{{ url('/pdf') }}" class="btn btn-success btn-xs" title="Download Invoice">
                                                    <span class="glyphicon glyphicon-download"></span>
                                                </a>
                                                <a href="{{ url('/order/edit-order/'.$todayOrder->id) }}" class="btn btn-primary btn-xs" title="Edit Order">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a href="{{ url('/order/delete-order/'.$todayOrder->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');" title="Order Delete">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->


                        </section><!-- End section 2 -->






                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection



@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>







    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
