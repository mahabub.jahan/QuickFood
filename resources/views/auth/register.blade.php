@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

        <div id="subheader">
            <div id="sub_content">
                <h1>User Registration Form</h1>


            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>User Registration</li>
            </ul>

        </div>
    </div><!-- Position -->



    <!-- Content ================================================== -->


    <div class="white_bg">
        <div class="container margin_60_35">
            <div class="main_title margin_mobile">
                <h2 class="nomargin_top">Please submit the form below</h2>

            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form  method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="row">


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name">First name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Mahabub Jahan" value="{{ old('first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">Last name</label>
                                    <input type="text" class="form-control" id="name_contact" name="last_name" placeholder="Shuvo" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>



                        <div class="row">

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email:</label>
                                    <input type="email" id="email" name="email" class="form-control " placeholder="shuvo@email.com" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-md-6 col-sm-6">
                                <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                    <label for="mobile_number">Mobile number:</label>
                                    <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="01722222222" value="{{ old('mobile_number') }}" required>
                                </div>
                                @if ($errors->has('mobile_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>


                        <div class="row">



                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city">City</label>
                                    <select name="city" class="form-control" id="city">

                                        <option value="" selected="">Select City</option>

                                        @foreach(\App\Http\Controllers\ManageAdmin\CityController::city() as $cities)
                                            <option value="{{ $cities->city_name }}">{{ $cities->city_name }}</option>
                                        @endforeach



                                    </select>

                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('living_address') ? ' has-error' : '' }}">
                                    <label for="living_address">Livid Address</label>
                                    <input type="text" id="living_address" name="living_address" class="form-control" placeholder="80, Vila, Dhaka-1215, Dhanmondi" value="{{ old('living_address') }}" required>

                                    @if ($errors->has('living_address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('living_address') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>
                        </div><!-- End row  -->



                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Create a password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password"  id="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password"  id="password_confirmation">
                                </div>
                            </div>
                        </div><!-- End row  -->


                        <div id="pass-info" class="clearfix"></div>


                        {{--<div class="row">
                            <div class="col-md-6">
                                <label><input name="mobile" type="checkbox" value="" class="icheck" checked>Accept <a href="#0">terms and conditions</a>.</label>
                            </div>
                        </div>--}}<!-- End row  -->


                        <hr style="border-color:#ddd;">
                        <div class="text-center">
                            <button class="btn_full_outline" type="submit">Submit</button>
                        </div>
                    </form>
                </div><!-- End col  -->
            </div><!-- End row  -->
        </div><!-- End container  -->
    </div><!-- End white_bg -->


    <!-- End Content =============================================== -->
@endsection


@section('script')

@endsection
