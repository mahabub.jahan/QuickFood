@extends('front.master')

@section('link')

    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

        <div id="subheader">
            <div id="sub_content">
                <h1>User Login</h1>


            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>User Login</li>
            </ul>

        </div>
    </div><!-- Position -->


    <div class="container margin_60_35">
        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Please Login</h2>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">


                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <label for="remember">
                                <input id="remember" name="remember"  {{ old('remember') ? 'checked' : '' }} type="checkbox" class="icheck">Remember Me
                            </label>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <button type="submit" class="btn_full_outline">
                                        Login
                                    </button>
                                </div>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>


                        </div>
                    </div>
                </form>
            </div><!-- End col  -->
        </div><!-- End row  -->
    </div><!-- End container  -->



    <!-- End Content =============================================== -->
@endsection





@section('script')

@endsection
