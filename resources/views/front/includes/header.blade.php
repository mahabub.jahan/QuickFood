<!-- Header ================================================== -->
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col--md-4 col-sm-4 col-xs-4">
                <a href="{{ url('/') }}" id="logo">
                    <img src="{{asset('front-end')}}/img/logo.png" width="190" height="23" alt="" data-retina="true" class="hidden-xs">
                    <img src="{{asset('front-end')}}/img/logo_mobile.png" width="59" height="23" alt="" data-retina="true"
                         class="hidden-lg hidden-md hidden-sm">
                </a>
            </div>
            <nav class="col--md-8 col-sm-8 col-xs-8">
                <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>

                <div class="main-menu">

                    <div id="header_menu">
                        <img src="{{asset('front-end')}}/img/logo.png" width="190" height="23" alt="" data-retina="true">
                    </div>

                    <a href="#" class="open_close" id="close_in">
                        <i class="icon_close"></i>
                    </a>
                    <ul>

                        <li class="submenu">
                            <a href="{{ url('/') }}" class="show-submenu">Home</a>
                        </li>

                        <li class="submenu">
                            <a href="{{ route('restaurant.list') }}" class="show-submenu">Restaurants</a>
                        </li>

                    {{--    <li><a href="blog.html">Blog</a></li>--}}

                        <li><a href="{{ route('aboutUs') }}">About us</a></li>
                        <li><a href="{{ route('faq') }}">Faq</a></li>






                        @guest
                            <li class="submenu">
                                <a href="#" class="show-submenu">Login<i class="icon-down-open-mini"></i></a>
                                <ul>

                                    <li><a href="{{ url('/login') }}"><i class="icon-login-1"></i> User</a></li>
                                    <li><a href="{{ route('restaurant.login') }}"><i class="icon-restaurant"></i> Restaurant Owner</a></li>

                                </ul>
                            </li>

                            <li class="submenu">
                                <a href="#" class="show-submenu">Register<i class="icon-down-open-mini"></i></a>
                                <ul>

                                    <li><a href="{{ url('/register') }}"><i class="icon-login-1"></i> User</a></li>
                                    <li><a href="{{ route('restaurant.register') }}"><i class="icon-restaurant"></i> Restaurant Owner</a></li>

                                </ul>
                            </li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>





                            @else
                                @if (\Illuminate\Support\Facades\Auth::guard('web')->check())
                                    <li class="submenu">
                                        <a href="#" class="show-submenu">{{ \Illuminate\Support\Facades\Auth::user()->last_name }}<i class="icon-down-open-mini"></i></a>
                                        <ul>

                                            <li><a href="{{ route('user.orders') }}">My Orders</a></li>
                                            <li><a href="{{ route('user.profile') }}">Profile</a></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>

                                        </ul>
                                    </li>





                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>



                                @elseif (\Illuminate\Support\Facades\Auth::guard('restaurant')->check())

                                    <li class="submenu">
                                        <a href="{{ route('restaurant.dashboard') }}" class="show-submenu">{{ \Illuminate\Support\Facades\Auth::user()->last_name }}<i class="icon-down-open-mini"></i></a>
                                        <ul>

                                            <li><a href="{{ route('restaurant-admin.profile') }}">Profile</a></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>


                                        </ul>
                                    </li>
                                @endif


                        @endguest
                                    @if( Session::get('grandTotal'))
                                        <li><a href="{{ url('/show-cart') }}"><span class="icon_cart"></span> TK. {{ Session::get('grandTotal') }}</a></li>
                                    @endif






                    </ul>
                </div><!-- End main-menu -->
            </nav>
        </div><!-- End row -->
    </div><!-- End container -->
</header>
<!-- End Header =============================================== -->
