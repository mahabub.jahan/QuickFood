@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll"
             <?php
                    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
                    $subHeaderImage = $subHeaderImage->subHeaderImage();
             ?>
             data-image-src="{{asset($subHeaderImage->sub_header_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Your Order List</h1>
                {{--<div><i class="icon_pin"></i> 135 Newtownards Road, Belfast, BT4 1AB</div>--}}
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('restaurant.list') }}">Restaurant List</a></li>
                <li><a href="{{ url('/restaurant/restaurant-menu/'.Session::get('restaurant_unique_id')) }}">Restaurant Menu</a></li>
                <li>Order Page</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-12">
                <div class="table-responsive shopping-cart-table">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td class="text-center">
                                Image
                            </td>
                            <td class="text-center">
                                Food Item
                            </td>
                            <td class="text-center">
                                Quantity
                            </td>
                            <td class="text-center">
                                Ingredients
                            </td>
                            <td class="text-center">
                                Price
                            </td>

                            <td class="text-center">
                                Action
                            </td>
                        </tr>
                        </thead>

                        <tbody>
                        @php($i=1)
                        @php($sum=0)
                        @forelse($cartProducts as $cartProduct)
                        <tr>
                            <td class="text-center">
                                <a href="{{asset($cartProduct->options->image)}}">

                                    <img src="{{asset($cartProduct->options->image)}}" alt="{{ $cartProduct->name }}"
                                         title="{{ $cartProduct->name }}" class="img-thumbnail" height="80" width="80">
                                </a>
                            </td>
                            <td class="text-center">
                            {{ $cartProduct->name }}
                            </td>

                            <td class="text-center">
                                <div class="input-group btn-block">
                                    <form id="update{{$i}}" action="{{ url('/update-cart-product') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="number" name="qty" style="width: 90px;" value="{{ $cartProduct->qty }}" min="1" size="1" class="form-control">
                                        <input type="hidden" value="{{ $cartProduct->rowId }}" name="rowId">
                                    </form>
                                </div>
                            </td>

                            <td class="text-center">
                                {{ $cartProduct->options->ingredient }}
                            </td>
                            <td class="text-center">
                                ৳ {{ $total = $cartProduct->price*$cartProduct->qty }}
                            </td>
                            <td class="text-center">
                                <button form="update{{$i}}" type="submit" title="Update" class="btn btn-default tool-tip">
                                    <span class="icon-cw-circle"></span>
                                </button>


                                <a href="{{ url('/delete-cart-product/'.$cartProduct->rowId) }}" class="btn btn-default tool-tip" title="Delete Chart Product">
                                    <span class="icon-trash-2"></span>
                                </a>


                            </td>
                        </tr>
                        @php($i++)

                        @php($sum = $sum + $total)
                        @empty
                            <h1>No Product Availabel</h1>
                        @endforelse

                        </tbody>

                    </table>

                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Sub Total</th>
                            <td>৳ {{ $sum }}</td>
                        </tr>


                       {{-- <tr>
                            <th>Delivery Fee</th>
                            <td>৳ {{ Session::get('delivery_fee')}} </td>
                        </tr>--}}

                        <tr>
                            <th>Grand Total</th>
                            <td>৳ {{ $grandTotal = ($sum /*+ Session::get('delivery_fee') */) }}</td>
                            {{ Session::put('grandTotal', $grandTotal) }}
                        </tr>
                    </table>


                    <table class="table table-bordered" id="cart_box">
                        <tr>

                            <td>
                                <a href="{{ url('/restaurant/restaurant-menu/'.Session::get('restaurant_unique_id')) }}" class="btn_full_outline"><i class="icon-right"></i> Add Other Items</a>
                            </td>
                            <td>

                                    @if(\Illuminate\Support\Facades\Auth::guard('web')->check())
                                        <a href="{{ url('/shipping-info') }}" class="btn_full"> Go to Checkout</a>
                                    @else

                                        @if( Session::get('minimum_order') > Session::get('grandTotal'))
                                            <a href="" onclick="myFunction()" class="btn_full"> Go to Checkout</a>
                                            <script>
                                                function myFunction() {
                                                    alert("Please Order Minimum TK. {{ Session::get('minimum_order')}}");
                                                }
                                            </script>
                                        @else
                                            <a href="{{ url('/checkout') }}" class="btn_full"> Go to Checkout</a>
                                        @endif
                                    @endif


                            </td>
                        </tr>
                    </table>


                </div>
            </div>


        </div>
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection




@section('script')

    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('front-end')}}/js/cat_nav_mobile.js"></script>
    <script>$('#cat_nav').mobileMenu();</script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAs_JyKE9YfYLSQujbyFToZwZy-wc09w7s"></script>
    <script src="{{asset('front-end')}}/js/map.js"></script>
    <script src="{{asset('front-end')}}/js/infobox.js"></script>
    <script src="{{asset('front-end')}}/js/ion.rangeSlider.js"></script>
    <script>
        $(function () {
            'use strict';
            $("#range").ionRangeSlider({
                hide_min_max: true,
                keyboard: true,
                min: 0,
                max: 15,
                from: 0,
                to: 5,
                type: 'double',
                step: 1,
                prefix: "Km ",
                grid: true
            });
        });
    </script>
@endsection
