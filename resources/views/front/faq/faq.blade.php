@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{asset('front-end')}}/css/skins/square/grey.css" rel="stylesheet">

@endsection

@section('subheader')



    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{asset($subHeaderImage->sub_header_image)}}" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>Frequently asked questions</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>FAQ</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div class="box_style_1" id="faq_box">
                        <ul id="cat_nav">
                            @foreach($faqTitles as $faqTitle)
                                <li><a href="#{{ $faqTitle->id }}" class="active">{{ $faqTitle->faq_title_name }}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- End box_style_1 -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                @foreach($faqTitles as $faqTitle)
                    @php($j=1)

                    @foreach($faqContents as $faqContent)
                        @if($faqContent->faq_title_id != $faqTitle->id)
                            @continue
                        @endif
                        @if($j==1)
                                @php($j++)
                                <h3 class="nomargin_top">{{ $faqTitle->faq_title_name }}</h3>
                        @endif

                <div class="panel-group" id="{{ $faqTitle->id }}">

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#{{ $faqTitle->id }}" href="#{{ $faqContent->id }}"><?php echo $faqContent->question ?><i class="indicator icon_plus_alt2 pull-right"></i></a>
                            </h4>
                        </div>

                        <div id="{{ $faqContent->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                @isset($faqContent->answer)

                                <?php echo $faqContent->answer ?>

                                @endisset

                                @if($faqContent->image == 0)

                                <div class="row magnific">

                                    <div class="col-md-12">
                                        <?php echo $faqContent->answer ?>
                                        <a href="<?php echo $faqContent->video_link;?>"  class="video_pop" title="Video title"><img src="{{asset($faqContent->image)}}" alt="" class="img-responsive"></a>
                                    </div>
                                </div><!-- End row-->

                                @endif
                            </div>
                        </div>
                    </div>


                </div><!-- End panel-group -->

                 @endforeach
                @endforeach



            </div><!-- End col-md-9 -->
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection





@section('script')
    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('front-end')}}/js/theia-sticky-sidebar.js"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>
    <script>
        $('#faq_box a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            var target = this.hash;
            var $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 120
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
    </script>

@endsection
