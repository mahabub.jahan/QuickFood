@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end')}}/css/skins/square/grey.css" rel="stylesheet">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{isset($restaurant->restaurant_image)}}" data-natural-width="1400" data-natural-height="470">
        <div id="subheader">
            <div id="sub_content">


                <div id="thumb"><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" ><img src="{{asset($restaurant->restaurant_logo)}}" alt=""></a></div>
                <div class="rating"><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">
                        <?php
                        $review = new \App\Http\Controllers\Front\RestaurantDetailsFrontController();
                        $rating = $review->restaurantRating($restaurant->restaurant_id);

                        ?>


                            @for($r=0 ; $r < 5; $r++)
                                @if ($r < intval($rating))
                                    <i class="icon_star voted"> </i>
                                @else
                                    <i class="icon_star"></i>

                                @endif
                            @endfor

                         (<small><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}">Read {{ $reviewCount }} reviews</a></small>)</a>
                </div>
                <h1><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">{{ $restaurant->restaurant_name }}</a></h1>
                <div><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">
                    <em>
                        @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allCuisines($restaurant->id) as $cuisines)
                            {{ $cuisines->cuisines_name }} /
                        @endforeach
                    </em></a>
                </div>
                <div><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;"><i class="icon_pin"></i> {{$restaurant->restaurant_address}} - <strong>Delivery charge:</strong> ৳  {{$restaurant->delivery_fee}}</a></div>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('restaurant.list') }}">Category</a></li>
                <li>Restaurant Menu List</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.list') }}" class="btn_side">Back to search</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        @foreach($menus as $menu)

                        <li>
                            <a href="#{{ $menu->id }}">{{ $menu->menu_name }}
                                <span>
                                   ({{  \App\Http\Controllers\Front\RestaurantFrontController::countMenuItem($menu->id) }})
                                </span>
                            </a>
                        </li>



                            {{--@foreach(\App\Http\Controllers\Front\RestaurantFrontController::allorders($menu->id) as $count)

                            @endforeach--}}

                        @endforeach
                    </ul>
                </div><!-- End box_style_1 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone"> {{ $restaurant->customer_care_number }}</a>
                    <small>{{ $restaurant->customer_cate_service_duration }}</small>
                </div>
            </div><!-- End col-md-3 -->

            <div class="col-md-6">
                <div class="box_style_2" id="main_menu">
                    <h2 class="inner">Menu</h2>

                    @foreach($menus as $menu)

                    <h3 class="nomargin_top" id="{{ $menu->id }}">{{ $menu->menu_name }}</h3>
                    <p>
                        {{ $menu->menu_description }}
                    </p>
                    <table class="table table-striped cart-list">
                        <thead>
                        <tr>
                            <th>
                                Item
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                Order
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @php($i=1)
                        @foreach($data as $datum)
                            @if($datum->menu_name != $menu->menu_name)
                                @continue
                            @endif

                                @if($datum->publication_status == 1)


                                    <tr>
                                        <td>
                                            <figure class="thumb_menu_list"><img src="{{asset($datum->item_image)}}" alt="thumb"></figure>
                                            <h5>{{$i++}}. {{ $datum->item_name }}</h5>
                                            <p>
                                                {{ $datum->item_description }}
                                            </p>
                                        </td>
                                        <td>
                                            <strong>৳ {{ $datum->item_price }}</strong>
                                        </td>

                                        <td class="options">
                                            <div class="dropdown dropdown-options">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="icon_plus_alt2"></i></a>
                                                <div class="dropdown-menu">
                                                    <form action="{{ url('add-to-cart') }}" method="POST">
                                                        {{ csrf_field() }}

                                                        <label>
                                                            <input  type="hidden" class="form-group" name="qty" value="1" min="1">
                                                            <input  type="hidden" name="item_id" value=" {{ $datum->id }}" >
                                                            <input  type="hidden" name="restaurant_unique_id" value=" {{ $restaurant->restaurant_id }}" >
                                                        </label>




                                                        @php($j=1)
                                                        @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allorders($datum->id) as $ingredient)

                                                            @if($j==1)
                                                                <h5>Add ingredients</h5>
                                                            @endif


                                                            <label>
                                                                <input type="checkbox" name="ingredient{{$j++}}" value="{{ $ingredient->id }}">{{ $ingredient->ingredient_name }} <span>+৳ {{ $ingredient->ingredient_price }}</span>
                                                            </label>
                                                        @endforeach


                                                        <button type="submit" class="add_to_basket">Add to cart</button>


                                                    </form>
                                                </div>
                                            </div>
                                        </td>



                                    </tr>
                                @endif

                            @endforeach

                        </tbody>
                    </table>
                    <hr>
                    @endforeach


                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box" >
                        <h3>Service Info <i class="icon_cart_alt pull-right"></i></h3>





                        <hr>
                        <table class="table table_summary">
                            <tbody>

                            <tr>
                                <td>
                                    Delivery Service<span class="pull-right"><?php if ($restaurant->delivery_service ){ echo '<i class="icon_check_alt2 ok text text-success"></i>';} else{echo '<i class="icon-cancel-circled2 text text-danger"></i>';} ?></span>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    Take away Service<span class="pull-right"><?php if ($restaurant->take_way_service ){ echo "<i class='icon_check_alt2 ok text text-success'></i>";} else{echo "<i class='icon-cancel-circled2 text text-danger'></i>";}?></span>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    Delivery fee <span class="pull-right">৳ {{ $restaurant->delivery_fee }}</span>

                                </td>

                            </tr>
                            <tr>
                                <td>
                                    Minimum Order <span class="pull-right">৳ {{ $restaurant->minimum_order }}</span>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                        <hr>

                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection





@section('script')

    <!-- SPECIFIC SCRIPTS -->
    <script  src="{{ asset('front-end')}}/js/cat_nav_mobile.js"></script>
    <script>$('#cat_nav').mobileMenu();</script>
    <script src="{{ asset('front-end')}}/js/theia-sticky-sidebar.js"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>
    <script>
        $('#cat_nav a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            var target = this.hash;
            var $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 70
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
    </script>
@endsection
