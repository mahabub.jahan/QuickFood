<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Restaurant Owner
            <small>Manage Restaurant Owner Info</small>
        </h1>

        @if($message = Session::get('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                {{ $message }}
            </div>
        @endif

        @if($message = Session::get('destroy'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                {{ $message }}
            </div>
        @endif

    </section>
    <!-- Main content -->
    <section class="content">




        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Unverified Restaurant Owner Info</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header table-responsive">
                                        <h3 class="box-title">Unverified Restaurant Owner Information Data Table</h3>



                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>SL. Id</th>
                                                <th>Restaurant Name</th>
                                                <th>City</th>
                                                <th>Verification Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($j=1)
                                            @foreach($unverifiedRestaurantOwners as $unverifiedRestaurantOwner)
                                                <tr>
                                                    <td>{{ $j++ }}</td>
                                                    <td>{{ $unverifiedRestaurantOwner->restaurant_name }}</td>
                                                    <td>{{ $unverifiedRestaurantOwner->city }}</td>

                                                    <td>
                                                        @if( $unverifiedRestaurantOwner->verify == 0)
                                                            <span class="label label-warning">Not Verify</span>
                                                        @endif
                                                    </td>


                                                    <td>
                                                        <a href="{{ url('/restaurant-owner/'.$unverifiedRestaurantOwner->restaurant_id.'/restaurant-owner-info') }}" class="btn btn-info btn-xs" title="View Restaurant Owner Details">
                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                        </a>

                                                        @if($unverifiedRestaurantOwner->verify == 0)
                                                            <a href="{{ url('/restaurant-owner/'.$unverifiedRestaurantOwner->restaurant_id.'/verify-restaurant-owner') }}" class="btn btn-warning btn-xs" title="Not Verified Restaurant">
                                                                <span class="glyphicon glyphicon-arrow-down"></span>
                                                            </a>
                                                        @endif


                                                        <a href="{{ url('/restaurant-owner/'.$unverifiedRestaurantOwner->restaurant_id.'/delete-restaurant-owner') }}" title="Delete Restaurant Owner" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>





        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Verified Restaurant Owner Info</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Verified Restaurant Owner Information Data Table</h3>


                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body table-responsive">
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>SL. Id</th>
                                                <th>Restaurant Name</th>
                                                <th>City</th>
                                                <th>Verification Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($j=1)
                                            @foreach($verifiedRestaurantOwners as $verifiedRestaurantOwner)
                                                <tr>
                                                    <td>{{ $j++ }}</td>
                                                    <td>{{ $verifiedRestaurantOwner->restaurant_name }}</td>
                                                    <td>{{ $verifiedRestaurantOwner->city }}</td>

                                                    <td>
                                                        @if( $verifiedRestaurantOwner->verify == 1)
                                                            <span class="label label-success">Verified</span>
                                                        @endif
                                                    </td>


                                                    <td>
                                                        <a href="{{ url('/restaurant-owner/'.$verifiedRestaurantOwner->restaurant_id.'/restaurant-owner-info') }}" class="btn btn-info btn-xs" title="View Restaurant Owner Details">
                                                            <span class="glyphicon glyphicon-eye-open"></span>
                                                        </a>


                                                        @if($verifiedRestaurantOwner->verify == 1)
                                                            <a href="{{ url('/restaurant-owner/'.$verifiedRestaurantOwner->restaurant_id.'/notverified-restaurant-owner') }}" class="btn btn-success btn-xs" title="Verify Restaurant">
                                                                <span class=" glyphicon glyphicon-arrow-up"></span>
                                                            </a>
                                                        @endif

                                                        @if($verifiedRestaurantOwner->popularity == 1)
                                                                <a href="{{ url('/restaurant-owner/'.$verifiedRestaurantOwner->restaurant_id.'/cancel-popularity-restaurant-owner') }}" class="btn btn-success btn-xs" title="Verified Restaurant Popularity">
                                                                    <span class="glyphicon glyphicon-menu-up"></span>
                                                                </a>
                                                            @else
                                                                <a href="{{ url('/restaurant-owner/'.$verifiedRestaurantOwner->restaurant_id.'/add-popularity-restaurant-owner') }}" class="btn btn-warning btn-xs" title="Cancel Verified Restaurant Popularity">
                                                                    <span class="glyphicon glyphicon-menu-down"></span>
                                                                </a>
                                                        @endif


                                                        <a href="{{ url('/restaurant-owner/'.$verifiedRestaurantOwner->restaurant_id.'/delete-restaurant-owner') }}" title="Delete Restaurant Owner" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>



    </section>
    <!-- /.content -->









@endsection


@section('script')
    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
            })
        })
    </script>
@endsection

