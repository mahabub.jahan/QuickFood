@extends('admin.master')

@section('body')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Quick Food</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">

            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{ url('/manage/user') }}"><span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Users</span>
                        <span class="info-box-number">{{ $userCount }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{ route('manage.restaurant.owner') }}"><span class="info-box-icon bg-red"><i class="glyphicon glyphicon-cutlery"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">Restaurant Request</span>
                        <span class="info-box-number">{{ $restaurantRequestCount }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{  route('manage.restaurant.owner') }}"><span class="info-box-icon bg-green"><i class="glyphicon glyphicon-cutlery"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Restaurant </span>
                        <span class="info-box-number">{{ $totalRestaurantCount }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->



            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{  route('manage.rider.admin') }}"><span class="info-box-icon bg-gray"><i class="fa fa-motorcycle"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Rider </span>
                        <span class="info-box-number">{{ $totalRider }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{  route('manage.rider.admin') }}"><span class="info-box-icon bg-aqua"><i class="fa fa-motorcycle"></i></span></a>

                    <div class="info-box-content">
                        <span class="info-box-text">New Register Rider </span>
                        <span class="info-box-number">{{ $newRegisterRider }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->




        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
@endsection