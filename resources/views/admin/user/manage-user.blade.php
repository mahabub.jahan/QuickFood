<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            User
            <small>Manage User</small>
        </h1>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header table-responsive">
                        <h3 class="box-title">User Information Data Table</h3>

                        @if($message = Session::get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                {{ $message }}
                            </div>
                        @endif

                        @if($message = Session::get('destroy'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                {{ $message }}
                            </div>
                        @endif

                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL. Id</th>
                                <th>User Name</th>
                                <th>Email</th>

                                <th>Mobile Number</th>
                                <th>City</th>
                                <th>Living Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($j=1)
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $j++ }}</td>
                                    <td>{{ $user->first_name }}{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>

                                    <td>{{ $user->mobile_number }}</td>
                                    <td>{{ $user->city }}</td>
                                    <td>{{ $user->living_address }}</td>

                                    <td>
                                        {{-- <a href="{{ url('/user/'.$user->id.'/user-info') }}" class="btn btn-info btn-xs" title="View user Details">
                                             <span class="glyphicon glyphicon-home"></span>
                                         </a>--}}
                                        {{-- <a href="{{ url('/user/'.$user->id.'/edit-user') }}" class="btn btn-info btn-xs" title="Edit user">
                                             <span class="glyphicon glyphicon-edit"></span>
                                         </a>--}}
                                        <a href="{{ url('/user/'.$user->id.'/delete-user') }}" title="Delete user" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection


@section('script')
    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection

