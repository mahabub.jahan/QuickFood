<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Faq Title
            <small>Manage Faq Title</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Faq Title Information Data Table</h3>

                        @if($message = Session::get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif

                        @if($message = Session::get('destroy'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL. Id</th>
                                <th>Faq Title Name</th>
                                <th>Publication Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i = 1 @endphp
                            @foreach($faqTitles as $faqTitle)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $faqTitle->faq_title_name }}</td>


                                    <td>
                                        @if($faqTitle->publication_status == 1)
                                            <span class="label label-success">Published</span>
                                        @else
                                            <span class="label label-warning">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($faqTitle->publication_status == 1)
                                            <a href="{{ url('/faq-title/unpublished-faq-title/'.$faqTitle->id) }}" class="btn btn-success btn-xs" title="Published">
                                                <span class="glyphicon glyphicon-arrow-up"></span>
                                            </a>
                                        @else
                                            <a href="{{ url('/faq-title/published-faq-title/'.$faqTitle->id) }}" class="btn btn-warning btn-xs" title="Unpublished">
                                                <span class="glyphicon glyphicon-arrow-down"></span>
                                            </a>
                                        @endif
                                        <a href="{{ url('/faq-title/edit-faq-title/'.$faqTitle->id) }}" class="btn btn-info btn-xs" title="Edit Title">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a href="{{ url('/faq-title/delete-faq-title/'.$faqTitle->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()

        })
    </script>
@endsection