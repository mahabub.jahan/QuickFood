<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            FAQ Title
            <small>Add FAQ Title</small>
        </h1>

    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- Horizontal Form -->
                <div class="box box-info">

                   @include('error')
                <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ url('/faq-title/new-faq-title') }}">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputName3" class="col-sm-3 control-label">FAQ Title Name</label>

                                <div class="col-sm-9">
                                    <input type="text" name="faq_title_name" required class="form-control" id="inputName3"
                                           placeholder="Add a Name">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publication Status</label>

                                <div class="col-sm-9">
                                    <select class="form-control" name="publication_status" required>
                                        <option value="">Select Publication Status</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-sm-offset-3">
                                <button type="submit" name="btn" class="btn btn-info">Save FAQ Title Info</button>
                                <button type="submit" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>



@endsection