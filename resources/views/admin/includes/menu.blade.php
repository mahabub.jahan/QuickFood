<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('/back-end/admin')}}/dist/img/logo.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
    </section>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <section>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            {{--Dashbord--}}
            <li class="active">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>




            <li class="treeview">
                <a href="#">
                    <i class="fa fa-question"></i>
                    <span>FAQ</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>

                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('add.faq.title') }}"><i class="fa fa-plus"></i> Add FAQ Title</a>
                    </li>
                    <li>
                        <a href="{{ route('manage.faq.title') }}"><i class="fa fa-tasks"></i> Manage FAQ Title</a>
                    </li>
                    <li class="treeview menu-open">
                        <a href="#">
                            <i class="fa fa-info"></i>FAQ Details
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display: block;">
                            <li>
                                <a href="{{ route('add.faq.content') }}"><i class="fa fa-plus"></i> Add FAQ Question</a>
                            </li>
                            <li>
                                <a href="{{ route('manage.faq.content') }}"><i class="fa fa-tasks"></i> Manage FAQ Question</a>
                            </li>
                        </ul>


                    </li>



                </ul>
            </li>





            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-image-o"></i>
                    <span>SubHeaderImage</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('add.subHeaderImage') }}"><i class="fa fa-plus"></i> Add Image</a>
                    </li>
                    <li>
                        <a href="{{ route('manage.subHeaderImage') }}"><i class="fa fa-tasks"></i> Manage Image</a>
                    </li>
                </ul>
            </li>



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-building"></i>
                    <span>City</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('add.city') }}"><i class="fa fa-plus"></i> Add City</a>
                    </li>
                    <li>
                        <a href="{{ route('manage.city') }}"><i class="fa fa-tasks"></i> Manage City</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="{{ route('manage.restaurant.owner') }}">
                    <i class="glyphicon glyphicon-cutlery"></i>
                    <span>Restaurant Owner</span>

                </a>

            </li>





            <li>
                <a href="{{ route('manage.user') }}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>

                </a>

            </li>


            <li>
                <a href="{{ route('manage.rider.admin') }}">
                    <i class="fa fa-motorcycle"></i>
                    <span>Rider</span>

                </a>

            </li>



        </ul>

    </section>
    <!-- /.sidebar -->
</aside>
