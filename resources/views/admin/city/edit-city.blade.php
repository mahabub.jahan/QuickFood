<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            City
            <small>Edit City</small>
        </h1>

    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- Horizontal Form -->
                <div class="box box-info">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ url('city/update-city/'.$city->id) }}">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('city_name') ? 'has-error' : '' }}">
                                <label for="city_name" class="col-sm-3 control-label">City Name</label>

                                <div class="col-sm-9">
                                    <input type="text" name="city_name" required class="form-control" id="city_name" value="{{ $city->city_name }}"
                                           placeholder="City Name..">
                                    @if($errors->has('city_name'))
                                        <span class="help-block">
                                        <strong class="text text-danger"> {{ $errors->first('city_name') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-sm-offset-3">
                                <button type="submit" name="btn" class="btn btn-info">Update City</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>



@endsection