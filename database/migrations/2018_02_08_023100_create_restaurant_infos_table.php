<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('restaurant_id');
            $table->string('restaurant_name');
            $table->text('restaurant_description')->nullable();

            $table->string('delivery_fee')->nullable();
            $table->string('minimum_order')->nullable();
            $table->string('delivery_service')->nullable();
            $table->string('take_way_service')->nullable();

            $table->string('customer_care_number')->nullable();
            $table->string('customer_cate_service_duration')->nullable();
            $table->string('city');


            $table->string('restaurant_address')->nullable();
            $table->text('delivery_zone')->nullable();

            $table->string('restaurant_logo')->nullable();
            $table->string('restaurant_image')->nullable();

            $table->string('popularity')->default("0");
            $table->string('verify')->default("0");



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_infos');
    }
}
;