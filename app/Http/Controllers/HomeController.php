<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function userOrder()
    {
         $orders = Order::where('customer_id','=',Auth::user()->id)
            ->where('order_status','=','Successful')
            ->get();

        return view('user.home', compact('orders'));
    }
    public static function shipping($id)
    {

        $shipping=DB::table('shippings')->select('shippings.delivery_schedule_day')
            ->leftJoin('orders','shippings.id','=','orders.shipping_id')
            ->where('orders.shipping_id','=',$id)
            ->get();
        return $shipping;
    }

    public static function restaurantName($id)
        {

            $name =DB::table('restaurant_infos')->select('restaurant_infos.restaurant_name')
                ->join('orders','restaurant_infos.restaurant_id','=','orders.restaurant_unique_id')
                ->where('orders.restaurant_unique_id','=',$id)
                ->get();
            return $name;
        }
















    public function userProfile()
    {
        $user = User::find(Auth::user()->id);

        return view('user.profile',compact('user'));
    }


    public function updateUserProfile(Request $request, $name)
    {
       $user = User::find(Auth::user()->id);
       $user->first_name = $request->first_name;
       $user->last_name = $request->last_name;
       $user->mobile_number = $request->mobile_number;
       $user->city = $request->city;
       $user->living_address = $request->living_address;
       $user->save();
       return redirect()->back()->with('message','Your Profile Updated Successfully !!');
    }


    public function showEmailPassword() {
        return view('user.change-email&password');
    }


    public function updateUserProfilePassword(Request $request) {

        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',

        ]);
        $user = User::find(Auth::user()->id);

        if(password_verify($request->old_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
            return redirect()->back()->with('message','Your Password Updated Successfully !!');
        } else{
            return redirect()->back();
        }


    }


    public function updateUserProfileEmail(Request $request) {

        $this->validate($request, [
            'old_email' => 'required|string|email',
            'email' => 'required|string|confirmed|email|max:100|unique:restaurants',

        ]);


        $user = User::find(Auth::user()->id);
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->with('message','Your Email Updated Successfully !!');
    }
}
