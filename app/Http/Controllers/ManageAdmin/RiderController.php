<?php

namespace App\Http\Controllers\ManageAdmin;

use App\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RiderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }






    public function manageRiderInfo(){

        $riders = Driver::orderBy('id', 'desc')->get();
        return view('admin.rider.manage-rider',compact('riders'));
    }

    public function notHireRider($data){
        $riderById = Driver::find($data);
        $riderById->hire = 0;
        $riderById->save();

        return redirect(route('manage.rider.admin'))->with('message','Hire Rider Successfully');


    }

    public function hireRider($data){
        $riderById = Driver::find($data);
        $riderById->hire = 1;
        $riderById->save();

        return redirect(route('manage.rider.admin'))->with('destroy','Hire Not Rider Successfully');


    }


    public function deleteRiderInfo($id)
    {
        $riderById = Driver::find($id);
        $riderById->delete();
        return redirect()->back()->with('destroy','Rider Info Delete Successfully');
    }
    // Faq Title End

}
