<?php

namespace App\Http\Controllers\ManageAdmin;

use App\Cuisine;
use App\DeliveryTime;
use App\Ingredient;
use App\Order;
use App\OrderDetail;
use App\Payment;
use App\Restaurant;
use App\RestaurantInfo;
use App\RestaurantMenu;
use App\RestaurantMenuItem;
use App\RestaurantOpenTime;
use App\RestaurantSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantOwnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    public function manageRestaurantOwner()
    {
        $unverifiedRestaurantOwners = RestaurantInfo::where('verify','=',0)->get();
        $verifiedRestaurantOwners = RestaurantInfo::where('verify','=',1)->get();
        return view('admin.restaurant-owner.manage-restaurant-owner',compact('unverifiedRestaurantOwners','verifiedRestaurantOwners'));
    }


    public function verifyRestaurantOwner($data){
        $restaurantById = RestaurantInfo::where('restaurant_id','=',$data)->first();
        $restaurantById->verify = 1;
        $restaurantById->save();

        return redirect(route('manage.restaurant.owner'))->with('message','Verify Restaurant Owner info successfully');


    }

     public function notverifyRestaurantOwner($data){
        $restaurantById = RestaurantInfo::where('restaurant_id','=',$data)->first();
        $restaurantById->verify = 0;
        $restaurantById->save();

        return redirect(route('manage.restaurant.owner'))->with('destroy','Cancel Verification Restaurant Owner info successfully');


    }


     public function verifyPopularityRestaurantOwner($data){
        $restaurantById = RestaurantInfo::where('restaurant_id','=',$data)->first();
        $restaurantById->popularity = 1;
        $restaurantById->save();

        return redirect(route('manage.restaurant.owner'))->with('message','Verify Restaurant Popularity info successfully');


    }

     public function cancelPopularityRestaurantOwner($data){
        $restaurantById = RestaurantInfo::where('restaurant_id','=',$data)->first();
        $restaurantById->popularity = 0;
        $restaurantById->save();

        return redirect(route('manage.restaurant.owner'))->with('destroy','Cancel  Restaurant Popularity info successfully');


    }






    public function viewRestaurantOwnerInfo($id){

        $restaurantInfo = RestaurantInfo::where('restaurant_id','=',$id)->first();
        $restaurantOwnerInfo = Restaurant::where('unique_id','=',$id)->first();
         $restaurantOpenTimeInfo = RestaurantOpenTime::where('restaurant_unique_id','=',$id)->first();
        $restaurantCuisines = Cuisine::where('restaurant_infos_id','=',$restaurantInfo->id)->get();
        $deliveryTimes = DeliveryTime::where('restaurant_unique_id','=',$id)->get();

        return view('admin.restaurant-owner.view-restaurant-owner-info',compact('restaurantInfo','restaurantOwnerInfo','restaurantOpenTimeInfo','restaurantCuisines','deliveryTimes'));
    }



    public function deleteRestaurantOwnerInfo($id)
    {
         $restaurantInfo = RestaurantInfo::where('restaurant_id','=',$id)->first();
        $restaurantOwnerInfo = Restaurant::where('unique_id','=',$id)->first();
         $restaurantOpenTimeInfo = RestaurantOpenTime::where('restaurant_unique_id','=',$id)->first();
         $restaurantCuisines = Cuisine::where('restaurant_infos_id','=',$restaurantInfo->id)->get();

         $restaurantMenus = RestaurantMenu::where('restaurant_unique_id','=',$id)->get();
         $restaurantMenuItems = RestaurantMenuItem::where('restaurant_unique_id','=',$id)->get();

         $ingredients = Ingredient::where('restaurant_unique_id','=',$id)->get();

         $orders = Order::where('restaurant_unique_id','=',$id)->get();

         $orderDetails = OrderDetail::where('restaurant_unique_id','=',$id)->get();

         $shippings = Order::where('restaurant_unique_id','=',$id)->get();

         $payments = Payment::where('restaurant_unique_id','=',$id)->get();

         //Delete Payment
        foreach ($payments as $payment){
            $payment->delete();
        }

        // Delete Sub Image
         $restaurantSubImages = RestaurantSubImage::where('restaurant_unique_id','=',$id)->get();
        foreach ($restaurantSubImages as $subImage){
            @unlink($subImage->sub_image);
            $subImage->delete();
        }

        // Delete All Shipping

        foreach ($shippings as $shipping){
            $shipping->delete();
        }

        // Delete Order
        foreach ($orderDetails as $orderDetail){
            $orderDetail->delete();
        }

        // Delete Orders
        foreach ($orders as $order){
            $order->delete();
        }

        // Delete Ingredient
        foreach ($ingredients as $ingredient){
            $ingredient->delete();
        }

        //Delete Menu Item
        foreach ($restaurantMenuItems as $restaurantMenuItem){
            $restaurantMenuItem->delete();
        }

        //Delete Menus

        foreach ($restaurantMenus as $menu){
            $menu->delete();
        }

        //Delete Cuisines
        foreach ($restaurantCuisines as $restaurantCuisine){
            $restaurantCuisine->delete();
        }

        // Delete Restaurant Owner
        $restaurantOpenTimeInfo->delete();

        // Delete Restaurant Owner Info
        $restaurantOwnerInfo->delete();

        // Delete Restaurant Info
        if ($restaurantInfo->restaurant_logo) {
            @unlink($restaurantInfo->restaurant_logo);
        }
        if ($restaurantInfo->restaurant_image) {
            @unlink($restaurantInfo->restaurant_image);
        }

        $restaurantInfo->delete();

        return redirect(route('manage.restaurant.owner'))->with('destroy','Restaurant all Info Delete Successfully');
    }
}
