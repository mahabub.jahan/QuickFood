<?php

namespace App\Http\Controllers\ManageAdmin;

use App\FaqTitel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    // For Faq title start
    public function addFaqTitleInfo()
    {
        return view('admin.faq.faq-title.add-faq-title');
    }


    public function saveFaqTitleInfo(Request $request)
    {

        $this->validate($request, [
            'faq_title_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'publication_status' => 'required',


        ]);


        $faqTitle = new FaqTitel();
         $faqTitle->faq_title_name = $request->faq_title_name;
         $faqTitle->publication_status = $request->publication_status;
         $faqTitle->save();



        return redirect('/faq-title/manage-faq-title')->with('message','Faq Title info add successfully');
    }


    public function manageFaqTitleInfo(){

        $faqTitles = FaqTitel::orderBy('id', 'desc')->get();
        return view('admin.faq.faq-title.manage-faq-title',compact('faqTitles'));
    }

    public function unpublishedFaqTitleInfo($data){
        $faqTitleById = FaqTitel::find($data);
        $faqTitleById->publication_status = 0;
        $faqTitleById->save();

        return redirect('/faq-title/manage-faq-title')->with('message','Faq Title info unpublished');


    }

    public function publishedFaqTitleInfo($data){
        $faqTitleById = FaqTitel::find($data);
        $faqTitleById->publication_status = 1;
        $faqTitleById->save();

        return redirect('/faq-title/manage-faq-title')->with('message','Faq Title info published');


    }


    public function editFaqTitleInfo($id)
    {

        $faqTitle = FaqTitel::find($id);
        return view('admin.faq.faq-title.edit-faq-title',compact('faqTitle'));
    }


    public function updateFaqTitleInfo(Request $request, $id){

        $this->validate($request, [
            'faq_title_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'publication_status' => 'required',


        ]);
        $faqTitleById = FaqTitel::find($id);
        $faqTitleById->faq_title_name = $request->faq_title_name;
        $faqTitleById->publication_status = $request->publication_status;
        $faqTitleById->save();
        return redirect('/faq-title/manage-faq-title')->with('message','Faq Title Update Successfully');
    }

    public function deleteFaqTitleInfo($id)
    {
        $faqTitleById = FaqTitel::find($id);
        $faqTitleById->delete();
        return redirect('/faq-title/manage-faq-title')->with('destroy','Faq Title Delete Successfully');
    }
    // Faq Title End





}
