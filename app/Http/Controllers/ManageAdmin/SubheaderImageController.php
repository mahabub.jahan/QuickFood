<?php

namespace App\Http\Controllers\ManageAdmin;

use App\SubheaderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class SubheaderImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

   public function showSubHeaderImagesForm(){
       return view('admin.subheaderimage.add-sub-header-image');
   }




    public function showSubHeaderImages()
    {
         $subHeaderImages = SubheaderImage::orderBY('id','desc')->get();
        return view('admin.subheaderimage.manage-sub-header-image',compact('subHeaderImages'));
    }


    public function saveSubHeaderImages(Request  $request)
    {
        $this->validate($request, [
            'sub_header_image' => 'required|image',

        ]);




        $subHeaderImage = $request->file('sub_header_image');

            $uniqueSubName = substr(bcrypt(md5(time())),'0','10');
            $subUniqueImageName =$uniqueSubName.'.'.$subHeaderImage->getClientOriginalExtension();
            $subHeaderImagedirectory = 'images/admin/sub-header-image/';
            $subHeaderImageUrl = $subHeaderImagedirectory.$subUniqueImageName;
            Image::make($subHeaderImage)->save($subHeaderImageUrl);

            $subHeaderImage = new SubheaderImage();
            $subHeaderImage->sub_header_image = $subHeaderImageUrl;
            $subHeaderImage->save();


        return redirect(route('manage.subHeaderImage'))->with('message', 'Sub Header  Image Inserted Successfully');
    }




    public function deleteSubHeaderImage($id)
    {

        $deleteSubHeaderImage = SubheaderImage::find($id);
        @unlink($deleteSubHeaderImage->sub_header_image);

        $deleteSubHeaderImage->delete();
        return redirect()->back()->with('destroy','Restaurant Slider Image Delete Successfully !');
    }
}
