<?php

namespace App\Http\Controllers\ManageAdmin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function manageCity() {
        $cities = City::all();
        return view('admin.city.manage-city',compact('cities'));
    }

    public function viewCityForm()
    {
        return view('admin.city.add-city');
    }


    public function addCity(Request $request)
    {
       $this->validate($request,[
           'city_name' => 'required|regex:/^[\pL\s\-]+$/u'

       ]);

       $city = new City();
       $city->city_name = $request->city_name;
       $city->save();

       return redirect(route('manage.city'))->with('message', 'City Add Successfully!!');
    }


    public function editCity($id)
    {
        $city = City::find($id);
        return view('admin.city.edit-city',compact('city'));
    }

    public function updateCity(Request $request, $id)
    {

        $this->validate($request,[
            'city_name' => 'required|regex:/^[\pL\s\-]+$/u'
        ]);


        $city = City::find($id);
        $city->city_name = $request->city_name;
        $city->save();

        return redirect(route('manage.city'))->with('message', 'City Info Updated Successfully!!');
    }



    public function deleteCity($id)
    {
        $city = City::find($id);
        $city->delete();
        return redirect(route('manage.city'))->with('destroy', 'City Info Deleted Successfully!!');
    }


    public static function city() {
        $cities = City::all();
        return $cities;
    }
}
