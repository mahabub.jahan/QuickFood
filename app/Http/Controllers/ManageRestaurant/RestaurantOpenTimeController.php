<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\RestaurantOpenTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RestaurantOpenTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }



    // Cuisines Section
    public function showRestaurantOpenTime()
    {
        $restaurantService = RestaurantOpenTime::orderBY('id','desc')->where('restaurant_unique_id',Auth::user()->unique_id)->first();
        return view('restaurant.restaurant-info.restaurant_open_time',compact('restaurantService'));
    }


    public function saveRestaurantOpenTime(Request  $request)
    {

        $this->validate($request, [

            'saturday' => 'required|string|max:100',
            'sunday' => 'required|string|max:100',
            'monday' => 'required|string|max:100',
            'tuesday' => 'required|string|max:100',
            'wednesday' => 'required|string|max:100',
            'thursday' => 'required|string|max:100',
            'friday' => 'required|string|max:100',


        ]);


        $restaurantService = RestaurantOpenTime::where('restaurant_unique_id',Auth::user()->unique_id)->first();

        $restaurantService->saturday = $request->saturday;
        $restaurantService->sunday = $request->sunday;
        $restaurantService->monday = $request->monday;
        $restaurantService->tuesday = $request->tuesday;
        $restaurantService->wednesday = $request->wednesday;
        $restaurantService->thursday = $request->thursday;
        $restaurantService->friday = $request->friday;
        $restaurantService->save();

        return redirect()->back()->with('message', 'Restaurant Open & Close Day Updated Successfully');
    }

}
