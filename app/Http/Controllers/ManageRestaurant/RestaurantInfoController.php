<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\Cuisine;
use App\RestaurantInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Image;
class RestaurantInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRestaurantInfoForm()
    {

         $restaurant = RestaurantInfo::where('restaurant_id',Auth::user()->unique_id)->first();
        return view('restaurant.restaurant-info.restaurant-info',compact('restaurant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editRestaurantInfo(Request $request)
    {




        $this->validate($request,[
            'restaurant_name' => 'required|string',
            'restaurant_description' => 'required|string',
            'delivery_fee' => 'required|numeric',
            'take_way_service' => 'required|string',
            'minimum_order' => 'required|numeric',
            'delivery_service' => 'required|string',
            'customer_care_number' => 'required|size:11|regex:/(01)[0-9]{9}/',
            'customer_cate_service_duration' => 'required|string',
            'city' => 'required|string|max:100',
            'restaurant_address' => 'required|string',
            'delivery_zone' => 'required|string',


        ]);





        $restaurantInfo = RestaurantInfo::find($request->id);

        $restaurant_logo = $restaurantInfo->restaurant_logo;
        $restaurant_image = $restaurantInfo->restaurant_image;


        if ($request->file('restaurant_logo')) {
            @unlink($restaurant_logo);


            $restaurant_logo = $request->file('restaurant_logo');
            $uniqueName = substr(bcrypt(time()),'0','10');
            $uniqueImageName = $uniqueName.'.'.$restaurant_logo->getClientOriginalExtension();
            $directory = 'images/restaurant-image/restaurant-logo/';
            $imageUrl = $directory.$uniqueImageName;
            // $restaurant_logo->move($directory.$uniqueImageName);
            Image::make($restaurant_logo)->save($imageUrl);


            $restaurantInfo->restaurant_logo = $imageUrl;
        } else {
            $restaurantInfo->restaurant_logo = $restaurant_logo;
        }

        if ($request->file('restaurant_image')) {
            @unlink($restaurant_image);


            $restaurant_image = $request->file('restaurant_image');
            $uniqueName = substr(md5(time()),'0','10');
            $uniqueImageName = $uniqueName.'.'.$restaurant_image->getClientOriginalExtension();
            $directory = 'images/restaurant-image/restaurant-cover/';
            $imageUrl = $directory.$uniqueImageName;
            // $restaurant_image->move($directory.$uniqueImageName);
            Image::make($restaurant_image)->save($imageUrl);

            $restaurantInfo->restaurant_image = $imageUrl;
        } else {
            $restaurantInfo->restaurant_image = $restaurant_image;
        }


        $restaurantInfo->restaurant_name = $request->restaurant_name;
        $restaurantInfo->restaurant_description = $request->restaurant_description;
        $restaurantInfo->delivery_fee = $request->delivery_fee;
        $restaurantInfo->minimum_order = $request->minimum_order;
        $restaurantInfo->delivery_service = $request->delivery_service;
        $restaurantInfo->take_way_service = $request->take_way_service;
        $restaurantInfo->customer_care_number = $request->customer_care_number;
        $restaurantInfo->customer_cate_service_duration = $request->customer_cate_service_duration;
        $restaurantInfo->restaurant_address = $request->restaurant_address;
        $restaurantInfo->delivery_zone = $request->delivery_zone;
        $restaurantInfo->save();
        return redirect()->back()->with('message', 'Restaurant Info Update Successfully');
    }



    // Cuisines Section
    public function showRestaurantCuisines()
    {
         $cuisines = Cuisine::orderBY('id','desc')->get();
        return view('restaurant.restaurant-info.cuisines',compact('cuisines'));
    }


    public function saveRestaurantCuisines(Request  $request)
    {
        $this->validate($request,[
            'cuisines_name' => 'required|regex:/^[\pL\s\-]+$/u'
        ]);

         $restaurant = RestaurantInfo::where('restaurant_id',Auth::user()->unique_id)->first();

        $cuisines = new Cuisine();
        $cuisines->restaurant_infos_id = $restaurant->id;
        $cuisines->cuisines_name = $request->cuisines_name;
        $cuisines->save();

        return redirect()->back()->with('message', 'Restaurant Cuisines Info Insert Successfully');
    }

    public function editRestaurantCuisines($id)
    {

           $cuisine = Cuisine::find($id);

        return view('restaurant.restaurant-info.edit-cuisines',compact('cuisine','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRestaurantCuisines(Request $request)
    {
        $this->validate($request,[
            'cuisines_name' => 'required|regex:/^[\pL\s\-]+$/u'
        ]);

        $cuisine = Cuisine::find($request->id);
        $cuisine->cuisines_name = $request->cuisines_name;
        $cuisine->save();
        return redirect()->action('ManageRestaurant\RestaurantInfoController@showRestaurantCuisines')->with('message', 'Restaurant Cuisines Info Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyRestaurantCuisines($id)
    {
         $cuisine = Cuisine::find($id);
        $cuisine->delete();

        return redirect()->back()->with('destroy','Restaurant Cuisine Delete Successfully !');
    }
}
