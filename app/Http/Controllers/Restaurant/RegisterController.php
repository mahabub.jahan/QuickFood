<?php

namespace App\Http\Controllers\Restaurant;

use App\City;
use App\Restaurant;
use App\RestaurantInfo;
use App\RestaurantOpenTime;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'restaurant/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:restaurant');
    }


    public function showRegistrationForm()
    {
        return view('restaurant.register');
    }




    public function register(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|string|email|max:255|unique:restaurants',
            'mobile_number' => 'required|size:11|regex:/(01)[0-9]{9}/',
            'restaurant_name' => 'required|string|max:100',
            'city' => 'required|string|max:100',
            'password' => 'required|string|min:6|confirmed',


        ]);



        $restaurantAuth = new Restaurant();
        $restaurantAuth->first_name = $request->first_name;
        $restaurantAuth->last_name = $request->last_name;
        $restaurantAuth->email = $request->email;
        $restaurantAuth->mobile_number = $request->mobile_number;
        $restaurantAuth->password = Hash::make($request->password);
        $unique_id= $restaurantAuth->unique_id = substr(bcrypt(time()),'0','15');
        $restaurantAuth->save();



        $restaurantInfo = new RestaurantInfo();
        $restaurantInfo->city = $request->city;
        $restaurantInfo->restaurant_id = $unique_id;
        $restaurantInfo->restaurant_name = $request->restaurant_name;
        $restaurantInfo->save();


        $restaurantOpen = new RestaurantOpenTime();
        $restaurantOpen->restaurant_unique_id = $unique_id;
        $restaurantOpen->save();




        // Attempt to log the user in
        if (Auth::guard('restaurant')->attempt(['email' => $request->email, 'password' => $request->password])) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('restaurant.dashboard'));
        }
        return redirect()->back();

    }



   }
