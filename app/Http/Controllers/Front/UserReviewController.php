<?php

namespace App\Http\Controllers\Front;

use App\UserReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserReviewController extends Controller
{
    public function userReview(Request $request) {
        $this->validate($request,[
            'food_review' => 'required',
            'price_review' => 'required',
            'punctuality_review' => 'required',
            'courtesy_review' => 'required',
            'review_text' => 'required',

        ]);
        $review = new UserReview();
        $review->restaurant_unique_id = $request->restaurant_unique_id;
        $review->user_id = $request->user_id;
        $review->food_review = $request->food_review;
        $review->price_review = $request->price_review;
        $review->punctuality_review = $request->punctuality_review;
        $review->courtesy_review = $request->courtesy_review;
        $review->review_text = $request->review_text;
        $review->save();

        return redirect()->back()->with('message','Review Insert Successfully !!');
    }
}
