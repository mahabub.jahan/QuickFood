<?php

namespace App\Http\Controllers\Front;

use App\RestaurantInfo;
use App\RestaurantMenu;
use App\RestaurantOpenTime;
use App\RestaurantSubImage;
use App\UserReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RestaurantDetailsFrontController extends Controller
{






    // Restaurant
    public function showRestaurantDetail($id)
    {
         $reviews = UserReview::where('restaurant_unique_id','=',$id)

            ->select(
                array(
                DB::raw('AVG(food_review) as food_review' ),
                DB::raw('AVG(price_review) as price_review' ),
                DB::raw('AVG(punctuality_review) as punctuality_review' ),
                DB::raw('AVG(courtesy_review) as courtesy_review' ),

            ))

            ->orderBy('user_reviews.id', 'DESC')
            ->groupBy('user_reviews.id')
            ->paginate(6);

         $otherRating = $this->getOtherRating($id);


        $restaurant = RestaurantInfo::where('restaurant_id','=', "$id")->first();
        $restaurantService = RestaurantOpenTime::orderBY('id','desc')->where('restaurant_unique_id',"$id")->first();
        $sliderImages = RestaurantSubImage::orderBY('id','desc')->where('restaurant_unique_id',"$id")->get();
        return view('front.restaurant.restaurant-detail',compact('restaurant','restaurantService','sliderImages','reviews','otherRating'));
    }



    public static function restaurantRating($id){
        $reviews = UserReview::where('restaurant_unique_id','=',$id)

            ->select(
                array(
                    DB::raw('AVG(food_review) as food_review' ),
                    DB::raw('AVG(price_review) as price_review' ),
                    DB::raw('AVG(punctuality_review) as punctuality_review' ),
                    DB::raw('AVG(courtesy_review) as courtesy_review' ),

                ))

            ->orderBy('user_reviews.id', 'DESC')
            ->groupBy('user_reviews.id')
            ->get();

         $count = UserReview::where('restaurant_unique_id','=',$id)->count();




        $foodReview = 0;
        $priceReview = 0;
        $punctualityReview = 0;
        $courtesyReview = 0;



        foreach ($reviews as $review){

            $foodReview = $review->food_review + $foodReview;
            $priceReview = $review->price_review + $priceReview;
            $punctualityReview = $review->punctuality_review + $punctualityReview;
            $courtesyReview = $review->courtesy_review + $courtesyReview;
        }
        if($count > 0) {
             $sum = (($foodReview/$count) +($priceReview/$count) +($punctualityReview/$count) +($courtesyReview/$count)) / 4;
            return intval($sum);
        } else {
            return 0;
        }

    }




    private function getRating($data) {
        $a = array();

        $restaurants1 = RestaurantInfo::where('verify','=','1')->get();
        foreach ($restaurants1 as $restaurant) {
            $reviews = UserReview::where('restaurant_unique_id','=',$restaurant->restaurant_id)

                ->select(
                    array(
                        DB::raw('AVG(food_review) as food_review' ),
                        DB::raw('AVG(price_review) as price_review' ),
                        DB::raw('AVG(punctuality_review) as punctuality_review' ),
                        DB::raw('AVG(courtesy_review) as courtesy_review' ),

                    ))

                ->orderBy('user_reviews.id', 'DESC')
                ->groupBy('user_reviews.id')
                ->get();

            $count = UserReview::where('restaurant_unique_id','=',$restaurant->restaurant_id)->count();

            $foodReview = 0;
            $priceReview = 0;
            $punctualityReview = 0;
            $courtesyReview = 0;

            foreach ($reviews as $review){

                $foodReview = $review->food_review + $foodReview;
                $priceReview = $review->price_review + $priceReview;
                $punctualityReview = $review->punctuality_review + $punctualityReview;
                $courtesyReview = $review->courtesy_review + $courtesyReview;
            }

            if($count > 0) {
                $sum = (($foodReview/$count) +($priceReview/$count) +($punctualityReview/$count) +($courtesyReview/$count)) / 4;
                if (intval($sum) == $data) {
                    array_push($a,$restaurant->restaurant_id);
                }
            }

        }

        return $a;
    }



    private function getOtherRating($id) {


            $count = UserReview::where('restaurant_unique_id','=',$id)->count();
            $reviews = UserReview::where('restaurant_unique_id','=',$id)

            ->select(
                array(
                    DB::raw('AVG(food_review) as food_review' ),
                    DB::raw('AVG(price_review) as price_review' ),
                    DB::raw('AVG(punctuality_review) as punctuality_review' ),
                    DB::raw('AVG(courtesy_review) as courtesy_review' ),

                ))

            ->orderBy('user_reviews.id', 'DESC')
            ->groupBy('user_reviews.id')
            ->get();

            $foodReview = 0;
            $priceReview = 0;
            $punctualityReview = 0;
            $courtesyReview = 0;

            foreach ($reviews as $review){

                $foodReview = $review->food_review + $foodReview;
                $priceReview = $review->price_review + $priceReview;
                $punctualityReview = $review->punctuality_review + $punctualityReview;
                $courtesyReview = $review->courtesy_review + $courtesyReview;
            }

            if($count > 0) {
                $foodReview = $foodReview/$count;
                $priceReview = $priceReview/$count;
                $punctualityReview = $punctualityReview/$count;
                $courtesyReview = $courtesyReview/$count;

               return $array = array("foodReview"=>"$foodReview", "priceReview"=>"$priceReview", "punctualityReview"=>"$punctualityReview","courtesyReview"=>"$courtesyReview");

            } else {
                return 0;
            }

    }












    public static function reviewUser($id)
    {

        $restaurant=DB::table('user_reviews')
            ->join('users','users.id','=','user_reviews.user_id')
            ->where('user_reviews.id','=',$id)
            ->select('user_reviews.review_text','user_reviews.created_at','users.first_name','users.last_name')
            ->get();
        return $restaurant;
    }



    public static function allCuisines($id)
    {

        $restaurant=DB::table('cuisines')->select('cuisines_name')
            ->leftJoin('restaurant_infos','restaurant_infos.id','=','cuisines.restaurant_infos_id')
            ->where('cuisines.restaurant_infos_id','=',$id)
            ->get();
        return $restaurant;
    }






// Restaurant
    public function showRestaurantMenu($id)
    {
        $restaurant = RestaurantInfo::where('restaurant_id','=', "$id")->first();
        $menus = RestaurantMenu::where('restaurant_unique_id', "$id")->get();



        $data = DB::table('restaurant_infos')
            ->join('restaurant_menus', 'restaurant_menus.restaurant_unique_id', '=', 'restaurant_infos.restaurant_id')
            ->join('restaurant_menu_items', 'restaurant_menu_items.restaurant_menu_id', '=', 'restaurant_menus.id')
            ->select('restaurant_infos.restaurant_name','restaurant_menus.*','restaurant_menu_items.*')
           ->where('restaurant_infos.restaurant_id','=',"$id")
            ->get();

        return view('front.restaurant.restaurant-menu-detail',compact('data','menus','restaurant'));
    }

    public static function allorders($id)
    {
        $data=DB::table('orders')->select('ingredient')
            ->leftJoin('items','items.id','=','orders.item_id')
            ->where('orders.item_id','=',$id)
            ->get();
        return $data;
    }


}
