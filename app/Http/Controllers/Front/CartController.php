<?php

namespace App\Http\Controllers\Front;

use App\Ingredient;
use App\RestaurantInfo;
use App\RestaurantMenuItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Cart;
use Illuminate\Support\Facades\Session;
use function Sodium\compare;

class CartController extends Controller
{

    public function addToCart(Request $request)
    {

        if (!Session::get('restaurant_unique_id')) {
            Session::put('restaurant_unique_id', $request->restaurant_unique_id);

        }

        if (Session::get('restaurant_unique_id') != $request->restaurant_unique_id) {
            Cart::destroy();
            Session::forget('restaurant_unique_id');


            if (!Session::get('restaurant_unique_id')) {
                Session::put('restaurant_unique_id', $request->restaurant_unique_id);

            }
        }

        $restaurant = RestaurantInfo::where('restaurant_id','=', Session::get('restaurant_unique_id'))->first();
        Session::put('minimum_order',$restaurant->minimum_order);
        Session::put('delivery_fee',$restaurant->delivery_fee);






        $menuItem = RestaurantMenuItem::find($request->item_id);
        $ingredients = Ingredient::where('restaurant_menu_items_id', '=', "$request->item_id")->get();
        $price = $menuItem->item_price;
        $i = 1;
        $ingredientsString = "";




        foreach ($ingredients as $ingredient) {
            $name="ingredient".$i;

            if (($request->$name)) {

                if ($ingredient->id == $request->ingredient.$i) {
                    $ingredientsString = "$ingredient->ingredient_name" . ", " . "$ingredientsString";
                    $price = $ingredient->ingredient_price + $price;


                }
            }
            $i++;
        }

        if ($ingredientsString == "") {
                $ingredientsString = "empty";
        }


        Cart::add([
            'id' => $menuItem->id,
            'name' => $menuItem->item_name,
            'price' => $price,
            'qty' => $request->qty,
            'options' => [
                'image' => $menuItem->item_image,
                'ingredient' => $ingredientsString


            ]

        ]);





        return redirect('/show-cart');
}


    public function showCart()
    {
        $cartProducts = Cart::content();

        return view('front.cart.show-cart',compact('cartProducts'));
    }


    public function updateCart(Request $request)
    {

        Cart::update($request->rowId, $request->qty);
        return redirect()->back();
    }

    public function back()
    {
        return redirect()->back();
    }


    public function deleteCart($id)
    {
        Cart::remove($id);

        return redirect('/show-cart');
    }
}
